pub mod display;
pub mod interpret;

#[derive(Debug, Clone, Copy)]
pub struct Position {
    pub line: i64,
    pub idx: i64,
}

#[derive(Debug, Clone, Copy)]
pub struct Location {
    pub start: Position,
    pub end: Position,
}

pub enum NumberType {
    TInteger,
    TFloat,
}

pub enum Type {
    TNil,
    TBoolean,
    TNumber(NumberType),
    TString,
    TFunction,
    TUserData,
    TThread,
    TTable,
}

#[derive(Clone, Copy)]
pub enum Number {
    NInteger(i64),
    NFloat(f64),
}

impl Number {
    pub fn as_float(self) -> f64 {
        match self {
            Number::NInteger(i) => i as f64,
            Number::NFloat(f) => f,
        }
    }
}

#[derive(Clone)]
pub enum Value {
    VNil,
    VBoolean(bool),
    VNumber(Number),
    VString(String),
}

pub type Name = String;

pub struct Var(Name);

pub enum UnOp {
    UNot,
    UNeg,
    USharp,
}

pub enum BinOp {
    BAnd,
    BOr,
    BAdd,
    BSub,
    BMul,
    BDiv,
    BFlDiv,
    BMod,
    BExp,
    BEq,
    BNeq,
    BLt,
    BLe,
    BGt,
    BGe,
    BDDot,
}

pub enum InnerExpr {
    EValue(Value),
    EVar(Var),
    EUnOp(UnOp, Box<Expr>),
    EBinOp(BinOp, Box<Expr>, Box<Expr>),
}

pub struct Expr(Location, InnerExpr);

pub enum Stmt {
    SEmpty,
    SAssign(Vec<Var>, Vec<Expr>),
    SBreak,
    SLabel(Name),
    SGoto(Name),
    SBlock(Block),
    SWhile(Expr, Block),
    SRepeat(Block, Expr),
    SIf(Expr, Block, Vec<(Expr, Block)>, Option<Block>),
    SFor(Name, Expr, Expr, Option<Expr>, Block),
    SIterator(Vec<Name>, Vec<Expr>, Block),
    SPrint(Expr),
}

pub struct Block(Vec<Stmt>);
