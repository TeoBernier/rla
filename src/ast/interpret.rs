use std::fmt::Formatter;

use crate::ast::display::PrettyPrintable;
use crate::ast::*;
use InnerComputationError as ICE;

#[derive(Debug)]
pub enum InnerComputationError {
    TypeError,
    IOError,
}

impl Into<ComputationError> for InnerComputationError {
    fn into(self) -> ComputationError {
        ComputationError::Error(self)
    }
}

#[derive(Debug)]
pub enum ComputationError {
    LocatedError(Location, InnerComputationError),
    Error(InnerComputationError),
}

impl ComputationError {
    pub fn default_location(self, loc: Location) -> Self {
        use ComputationError as CE;
        match self {
            CE::Error(err) => CE::LocatedError(loc, err),
            CE::LocatedError(..) => self,
        }
    }
}

pub trait Computable {
    fn compute(&self) -> Result<Value, ComputationError>;
}

impl Computable for Value {
    fn compute(&self) -> Result<Value, ComputationError> {
        Ok(self.clone())
    }
}

impl Computable for InnerExpr {
    fn compute(&self) -> Result<Value, ComputationError> {
        use BinOp as B;
        use InnerExpr as IE;
        use Number as N;
        use Value as V;
        match self {
            IE::EBinOp(binop, e1, e2) => match (e1.compute()?, e2.compute()?) {
                (V::VBoolean(b1), V::VBoolean(b2)) => match binop {
                    B::BAnd => Ok(V::VBoolean(b1 && b2)),
                    B::BOr => Ok(V::VBoolean(b1 || b2)),
                    // Handle eq and neq ?
                    _ => Err(ICE::TypeError.into()),
                },
                (V::VNumber(N::NInteger(n1)), V::VNumber(N::NInteger(n2))) => match binop {
                    B::BAdd => Ok(V::VNumber(N::NInteger(n1 + n2))),
                    B::BSub => Ok(V::VNumber(N::NInteger(n1 - n2))),
                    B::BMul => Ok(V::VNumber(N::NInteger(n1 * n2))),
                    B::BDiv => Ok(V::VNumber(N::NInteger(n1 / n2))),
                    B::BFlDiv => Ok(V::VNumber(N::NFloat(n1 as f64 / n2 as f64))),
                    B::BMod => Ok(V::VNumber(N::NInteger(n1 % n2))),
                    B::BExp => Ok(V::VNumber(N::NFloat((n1 as f64).powi(n2 as i32)))),
                    B::BEq => Ok(V::VBoolean(n1 == n2)),
                    B::BNeq => Ok(V::VBoolean(n1 != n2)),
                    B::BLt => Ok(V::VBoolean(n1 < n2)),
                    B::BLe => Ok(V::VBoolean(n1 <= n2)),
                    B::BGt => Ok(V::VBoolean(n1 > n2)),
                    B::BGe => Ok(V::VBoolean(n1 >= n2)),
                    _ => Err(ICE::TypeError.into()),
                },
                (V::VNumber(n1), V::VNumber(n2)) => {
                    let f1 = n1.as_float();
                    let f2 = n2.as_float();
                    match binop {
                        B::BAdd => Ok(V::VNumber(N::NFloat(f1 + f2))),
                        B::BSub => Ok(V::VNumber(N::NFloat(f1 - f2))),
                        B::BMul => Ok(V::VNumber(N::NFloat(f1 * f2))),
                        B::BDiv => Ok(V::VNumber(N::NFloat(f1 / f2))),
                        B::BFlDiv => Ok(V::VNumber(N::NFloat(f1 / f2))),
                        B::BMod => Ok(V::VNumber(N::NFloat(f1 % f2))),
                        B::BExp => Ok(V::VNumber(N::NFloat(f1.powf(f2)))),
                        B::BEq => Ok(V::VBoolean(f1 == f2)),
                        B::BNeq => Ok(V::VBoolean(f1 != f2)),
                        B::BLt => Ok(V::VBoolean(f1 < f2)),
                        B::BLe => Ok(V::VBoolean(f1 <= f2)),
                        B::BGt => Ok(V::VBoolean(f1 > f2)),
                        B::BGe => Ok(V::VBoolean(f1 >= f2)),
                        _ => Err(ICE::TypeError.into()),
                    }
                }
                (v1, v2) if matches!(binop, B::BDDot) => {
                    Ok(V::VString(v1.to_string() + &v2.to_string()))
                }
                _ => Err(ICE::TypeError.into()),
            },
            IE::EValue(v) => v.compute(),
            IE::EUnOp(op, e) => match (op, e.compute()?) {
                (UnOp::UNot, V::VBoolean(b)) => Ok(V::VBoolean(!b)),
                (UnOp::UNeg, V::VNumber(N::NInteger(i))) => Ok(V::VNumber(N::NInteger(-i))),
                (UnOp::UNeg, V::VNumber(N::NFloat(x))) => Ok(V::VNumber(N::NFloat(-x))),
                (UnOp::USharp, V::VString(s)) => Ok(V::VNumber(N::NInteger(s.len() as i64))),
                _ => Err(ICE::TypeError.into()),
            },
            IE::EVar(v) => unimplemented!("{}", v.pretty()),
        }
    }
}

impl Computable for Expr {
    fn compute(&self) -> Result<Value, ComputationError> {
        self.1.compute().map_err(|err| err.default_location(self.0))
    }
}

pub struct State<'a, 'b> {
    f: &'a mut Formatter<'b>,
}

pub trait Interpretable {
    fn interpret<'a, 'b>(&self, state: &mut State<'a, 'b>) -> Result<(), ComputationError>;
}

impl Interpretable for Stmt {
    fn interpret<'a, 'b>(&self, state: &mut State<'a, 'b>) -> Result<(), ComputationError> {
        match self {
            Stmt::SEmpty => Ok(()),
            Stmt::SAssign(_, _) => todo!(),
            Stmt::SBreak => todo!(),
            Stmt::SLabel(_) => todo!(),
            Stmt::SGoto(_) => todo!(),
            Stmt::SBlock(b) => b.interpret(state),
            Stmt::SWhile(e, b) => {
                while matches!(e.compute()?, Value::VBoolean(true)) {
                    b.interpret(state)?
                }
                Ok(())
            }
            Stmt::SRepeat(b, e) => {
                b.interpret(state)?;
                while matches!(e.compute()?, Value::VBoolean(true)) {
                    b.interpret(state)?
                }
                Ok(())
            }
            Stmt::SIf(_, _, _, _) => todo!(),
            Stmt::SFor(_, _, _, _, _) => todo!(),
            Stmt::SIterator(_, _, _) => todo!(),
            Stmt::SPrint(e) => PrettyPrintable::fmt(e, state.f).map_err(|_| ICE::IOError.into()),
        }
    }
}

impl Interpretable for Block {
    fn interpret<'a, 'b>(&self, state: &mut State<'a, 'b>) -> Result<(), ComputationError> {
        self.0.iter().try_fold((), |_, stmt| stmt.interpret(state))
    }
}
