use crate::ast::*;
use std::fmt::{self, Display, Formatter};

pub struct PrettyPrinter<'a, T: PrettyPrintable + ?Sized>(&'a T);
pub trait PrettyPrintable {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result;
    fn pretty<'a>(&'a self) -> PrettyPrinter<'a, Self> {
        PrettyPrinter(&self)
    }
}

impl<'a, T: PrettyPrintable + ?Sized> Display for PrettyPrinter<'a, T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl<T: PrettyPrintable> PrettyPrintable for &T {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        (*self).fmt(f)
    }
}

impl PrettyPrintable for Var {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl PrettyPrintable for UnOp {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            UnOp::UNot => write!(f, "not "),
            UnOp::UNeg => write!(f, "-"),
            UnOp::USharp => write!(f, "#"),
        }
    }
}

impl PrettyPrintable for BinOp {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            BinOp::BAnd => write!(f, " and "),
            BinOp::BOr => write!(f, " or "),
            BinOp::BAdd => write!(f, "+"),
            BinOp::BSub => write!(f, "-"),
            BinOp::BMul => write!(f, "*"),
            BinOp::BDiv => write!(f, "/"),
            BinOp::BFlDiv => write!(f, "//"),
            BinOp::BMod => write!(f, "%"),
            BinOp::BExp => write!(f, "^"),
            BinOp::BLt => write!(f, "<"),
            BinOp::BLe => write!(f, "<="),
            BinOp::BGt => write!(f, ">"),
            BinOp::BGe => write!(f, ">="),
            BinOp::BEq => write!(f, "=="),
            BinOp::BNeq => write!(f, "~="),
            BinOp::BDDot => write!(f, ".."),
        }
    }
}

impl Display for Number {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Number::NInteger(x) => write!(f, "{}", x),
            Number::NFloat(x) => write!(f, "{}", x),
        }
    }
}

impl Display for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Value::VNil => write!(f, "nil"),
            Value::VBoolean(x) => write!(f, "{}", x),
            Value::VNumber(x) => write!(f, "{}", x),
            Value::VString(x) => write!(f, "{}", x),
        }
    }
}

impl PrettyPrintable for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}

impl PrettyPrintable for InnerExpr {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use InnerExpr as IE;
        match self {
            IE::EValue(x) => write!(f, "{}", x.pretty()),
            IE::EVar(x) => write!(f, "{}", x.pretty()),
            IE::EUnOp(op, e) => write!(f, "{}{}", op.pretty(), e.pretty()),
            IE::EBinOp(op, e1, e2) => write!(f, "{}{}{}", e1.pretty(), op.pretty(), e2.pretty()),
        }
    }
}

impl PrettyPrintable for Expr {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        PrettyPrintable::fmt(&self.1, f)
    }
}

// used below to pretty vecs
impl<T: PrettyPrintable> PrettyPrintable for [T] {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        if let Some(first) = self.first() {
            write!(f, "{}", first.pretty())?;
            for pp in self.iter().skip(1) {
                write!(f, ", {}", pp.pretty())?;
            }
        }
        Ok(())
    }
}

impl PrettyPrintable for String {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(&self, f)
    }
}

impl PrettyPrintable for Stmt {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Stmt::SEmpty => Ok(()),
            Stmt::SAssign(vars, exprs) => writeln!(f, "{} = {}", vars.pretty(), exprs.pretty()),
            Stmt::SBreak => writeln!(f, "break"),
            Stmt::SLabel(l) => writeln!(f, "::{}::", l),
            Stmt::SGoto(l) => writeln!(f, "goto {}", l),
            Stmt::SBlock(b) => writeln!(f, "{}", b.pretty()),
            Stmt::SWhile(e, b) => writeln!(f, "while {} do\n{}end", e.pretty(), b.pretty()),
            Stmt::SRepeat(b, e) => writeln!(f, "repeat\n{}until {}", b.pretty(), e.pretty()),
            Stmt::SIf(e, b, lebl, ob) => {
                write!(f, "if {} then {}", e.pretty(), b.pretty())?;
                for (e, b) in lebl {
                    write!(f, "elseif {} then {}", e.pretty(), b.pretty())?;
                }
                if let Some(b) = ob {
                    write!(f, "else {}", b.pretty())?;
                };
                writeln!(f, "end")
            }
            Stmt::SFor(n, e1, e2, oe, b) => {
                write!(f, "for {} = {}, {}", n, e1.pretty(), e2.pretty())?;
                if let Some(e) = oe {
                    write!(f, ", {}", e.pretty())?;
                };
                write!(f, "do\n{}end", b.pretty())
            }
            Stmt::SIterator(nl, lel, b) => writeln!(
                f,
                "for {} = {} do \n{}end",
                nl.pretty(),
                lel.pretty(),
                b.pretty()
            ),
            Stmt::SPrint(e) => writeln!(f, "print({})", e.pretty()),
        }
    }
}

impl PrettyPrintable for Block {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}
