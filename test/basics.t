basic operations:
  $ dune exec ola basics.lua
  typing checks ...
  interprete ...
  nil
  true
  false
  false
  true
  false
  true
  42
  42
  42
  42
  -42
  -42
  -1
  -4
  -38
  -1
  -4
  100
  false
  true
  false
  true
  true
  false
  42.
  42.
  0.42
  0.42
  421.
  421.
  4.21
  421.
  421.
  4.21
  420.
  -42.
  -0.42
  -421.
  3.5
  3.5
  4.
  -42.
  -420.
  true
  true
  true
  2.
  2.
  2.
  2.
  0.45045045045
  inf
  16.
  16.
  16.
  16.
  18.9746319906
  0.
  1.
  0
  1
  0.
  1.
  0.
  1.
  4.
  1.0763
  4.
  -1.0763
  inf
  
  my string 123
  ²&é"#'{}[]()-|èçàù%*!§/:;,~+-*/=£$¤
  
  my string 123
  ²&é"#'{}[]()-|èçàù%*!§/:;,~+-*/=£$¤
  my string
  mystring
  my string
  mystring
  my string123
  my string123.345
  123456
  123.456789.
  123456.789
  123.456789
