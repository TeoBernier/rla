a = 42
print(a)

a1, a2, a3 = 40, 41, 40+2
print(a1)
print(a2)
print(a3)

a1, a2, a3 = 40, 41
print(a1)
print(a2)
print(a3)

a1, a2 = 40, 41, 42
print(a1)
print(a2)
print(a3)
